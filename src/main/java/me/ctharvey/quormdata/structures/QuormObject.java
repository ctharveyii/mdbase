/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.quormdata.structures;

import me.ctharvey.quormdata.factories.QuormObjectFactory;
import com.adamantdreamer.quorm.define.Auto;
import com.adamantdreamer.quorm.define.Id;
import com.adamantdreamer.quorm.define.Ignore;
import com.adamantdreamer.quorm.define.Size;
import com.adamantdreamer.quorm.define.Var;

/**
 *
 * @author thronecth
 */
public abstract class QuormObject {

    @Ignore
    private transient Class clazz;

    @Ignore
    private transient QuormObjectFactory factory;

    public abstract Class getClassType();

    public static class NameTooLongException extends Exception {

        public NameTooLongException() {
            super("Your object name is longer than 32 characters.");
        }
    }

    public boolean create() {
        return false;
    }

    ;

    public abstract boolean update();

    public abstract boolean delete();

    @Override
    public abstract Object clone();

    public QuormObject() {
    }

    public static abstract class QuormUniqueIDObject extends QuormObject {

        @Id
        @Auto
        protected int id;

        public QuormUniqueIDObject() {
        }

        public QuormUniqueIDObject(int id) {
            this.id = id;
        }

        public static abstract class QuormUniqueNameObject extends QuormUniqueIDObject {

            @Var
            @Size(32)
            protected String name;

            public QuormUniqueNameObject(int id) {
                super(id);
            }

            public QuormUniqueNameObject() {
            }

            public void setName(String name) throws NameTooLongException {
                if (name == null || "".equals(name)) {
                    throw new NameTooLongException();
                }
                if (name.length() > 32) {
                    throw new NameTooLongException();
                }
                this.name = name;
            }

            public String getName() {
                return name;
            }

        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

    }
}
