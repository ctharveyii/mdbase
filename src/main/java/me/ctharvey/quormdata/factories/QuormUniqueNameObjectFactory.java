/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.quormdata.factories;

import me.ctharvey.quormdata.structures.QuormObject;
import me.ctharvey.quormdata.structures.QuormObject.QuormUniqueIDObject.QuormUniqueNameObject;
import com.adamantdreamer.quorm.core.DAO;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.mdbase.database.QuormHandler;

/**
 *
 * @author thronecth
 * @param <K>
 */
public abstract class QuormUniqueNameObjectFactory<K extends QuormUniqueNameObject> extends QuormObjectFactory {

    protected final Map<String, Integer> nameIndex = new HashMap();

    public QuormUniqueNameObjectFactory(QuormHandler handler, Class clazz) {
        super(handler, clazz);
    }

    public K getFromName(String name) throws Exception {
        if (nameIndex.containsKey(name)) {
            Integer id = nameIndex.get(name);
            return (K) getIndex().get(id);
        }
        throw new Exception("Factory does not contain item by name: " + name);
    }

    @Override
    public void init() {
        try {
            List<K> allFromDB = getAllFromDB();
            if (allFromDB != null && !allFromDB.isEmpty()) {
                for (K object : allFromDB) {
                    index.put(object.getId(), object);
                    nameIndex.put(object.getName(), object.getId());
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(QuormObjectFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean add(QuormObject.QuormUniqueIDObject object) throws SQLException, InvalidQuormObjectException {
        if (!(object instanceof QuormUniqueNameObject)) {
            throw new InvalidQuormObjectException("You must provide an object that extends QuormUniqueNameObject");
        }
        QuormUniqueNameObject obj = (QuormUniqueNameObject) object;
        if (obj.getName() == null || obj.getName().isEmpty()) {
            throw new InvalidQuormObjectException("You must provide a valid name for a quormnameobject.");
        }
        if (super.add(object)) {
            nameIndex.put(((QuormUniqueNameObject) object).getName(), object.getId());
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean delete(QuormObject.QuormUniqueIDObject object) throws SQLException, InvalidQuormObjectException {
        if (!(object instanceof QuormUniqueNameObject)) {
            throw new InvalidQuormObjectException("You must provide an object that extends QuormUniqueNameObject");
        }
        if (super.delete(object)) {
            nameIndex.remove(((QuormUniqueNameObject) object).getName());
            return true;
        } else {
            return false;
        }
    }

    public static class InvalidQuormObjectException extends Exception {

        public InvalidQuormObjectException(String string) {
            super(string);
        }
    }

}
