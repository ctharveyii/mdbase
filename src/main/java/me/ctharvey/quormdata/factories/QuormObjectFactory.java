/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.quormdata.factories;

import me.ctharvey.quormdata.structures.QuormObject;
import me.ctharvey.quormdata.structures.QuormObject.QuormUniqueIDObject;
import me.ctharvey.quormdata.structures.QuormObject.QuormUniqueIDObject.QuormUniqueNameObject;
import com.adamantdreamer.quorm.core.DAO;
import com.adamantdreamer.quorm.core.TableDAO;
import com.adamantdreamer.quorm.query.Query;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.mdbase.database.QuormHandler;
import me.ctharvey.mdbase.MDBase;
import org.bukkit.Bukkit;

/**
 *
 * @author thronecth
 * @param <K>
 */
public abstract class QuormObjectFactory<K extends QuormUniqueIDObject> {

    protected QuormHandler handler;
    protected Class clazz;
    private Query<K> query;

    public QuormObjectFactory(QuormHandler handler, Class clazz) {
        this(handler, clazz, false);
    }

    public QuormObjectFactory(QuormHandler handler, Class clazz, boolean hasFallback) {
        this.handler = handler;
        if (clazz == null) {
            MDBase.getBase().logError("Class is null.");
            if (!hasFallback) {
                MDBase.getBase().logError("No fallback so shutting down.");
                Bukkit.getServer().shutdown();
            }
            return;
        }
        if (this.handler == null || !this.handler.isDbConnected()) {
            MDBase.getBase().logError("Handler is null or not connected.");
            if (!hasFallback) {
                MDBase.getBase().logError("No fallback so shutting down.");
                Bukkit.getServer().shutdown();
            }
            return;
        }
        this.clazz = clazz;
        if (getDAO() == null) {
            MDBase.getBase().logError("DAO IS NULL!");
            if (!hasFallback) {
                MDBase.getBase().logError("No fallback so shutting down.");
                Bukkit.getServer().shutdown();
            }
        } else {
            this.handler.addSchema(clazz);
            this.query = getDAO().read(clazz);
        }
    }

    protected final Map<Integer, K> index = new HashMap<>();

    protected void init() {
        try {
            List<K> allFromDB = getAllFromDB();
            if (allFromDB != null && !allFromDB.isEmpty()) {
                for (K object : allFromDB) {
                    index.put(object.getId(), object);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(QuormObjectFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public <T extends K> boolean add(T object) throws SQLException, QuormUniqueNameObjectFactory.InvalidQuormObjectException {
        getDAO().create(object);
        index.put(object.getId(), (T) object);
        return true;
    }

    public <T extends K, E extends T> boolean add(Class<T> clazz, E object) throws SQLException, QuormUniqueNameObjectFactory.InvalidQuormObjectException {
        TableDAO<T> table = handler.getDao().get(clazz);
        table.create(object);
        index.put(object.getId(), object);
        return true;
    }

    public List<K> getAllFromDB() throws SQLException {
        return this.query.read(new Object[]{}).readAll();
    }

    public abstract Map<Integer, ? extends K> getIndex();

    public <T extends K> K get(Class<T> clazz, int id) {
        if (index.containsKey(id)) {
            return index.get(id);
        } else {
            try {
                T newInstance;
                try {
                    newInstance = clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException ex) {
                    Logger.getLogger(QuormObjectFactory.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
                newInstance.setId(id);
                T read = getDAO().read(newInstance);
                if (read == null) {
                    MDBase.getBase().log(clazz.getName() + " was a null read from database with id " + id);
                }
                index.put(id, read);
                return read;
            } catch (SQLException ex) {
                Logger.getLogger(QuormObjectFactory.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
    }

    public K get(int id) {
        if (index.containsKey(id)) {
            return index.get(id);
        } else {
            try {
                K newInstance;
                try {
                    newInstance = (K) clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException ex) {
                    Logger.getLogger(QuormObjectFactory.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
                newInstance.setId(id);
                K read = getDAO().read(newInstance);
                if (read == null) {
                    MDBase.getBase().log(clazz.getName() + " was a null read from database with id " + id);
                }
                index.put(id, read);
                return read;
            } catch (SQLException ex) {
                Logger.getLogger(QuormObjectFactory.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
    }

    public boolean update(K object) throws SQLException, QuormUniqueNameObjectFactory.InvalidQuormObjectException {
        if (getDAO().update(object)) {
            index.put(object.getId(), object);
            return true;
        }
        return false;
    }

    public <T extends K, E extends T> boolean update(Class<T> clazz, E object) throws SQLException, QuormUniqueNameObjectFactory.InvalidQuormObjectException {
        TableDAO<T> get = handler.getDao().get(clazz);
        if (get.update(object)) {
            index.put(object.getId(), object);
            return true;
        }
        return false;
    }

    public boolean delete(K object) throws SQLException, QuormUniqueNameObjectFactory.InvalidQuormObjectException {
        return getDAO().delete(object);
    }

    public <T extends K, E extends T> boolean delete(Class<T> clazz, E object) throws SQLException, QuormUniqueNameObjectFactory.InvalidQuormObjectException {
        TableDAO<T> get = getDAO().get(clazz);
        return get.delete(object);
    }

    public Collection<K> getAllFromIndex() {
        return index.values();
    }

    public final DAO getDAO() {
        return this.handler.getDao();
    }

}
