/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.ctharvey.quormdata.factories;

import me.ctharvey.quormdata.structures.QuormObject;
import java.sql.SQLException;

/**
 *
 * @author thronecth
 * @param <K>
 */
public interface QuormObjectFactoryInterface<K extends QuormObject> {
    
    public boolean add(K object) throws SQLException;
    public boolean delete(K object) throws SQLException ;
    public boolean update(K object) throws SQLException ;
    
    
}
