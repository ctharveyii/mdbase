/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.multiserver;

import me.ctharvey.mdbase.MDBase;
import me.ctharvey.multiserv.client.MultiServeClient;
import me.ctharvey.multiserv.client.PacketHandler;
import me.ctharvey.multiserv.packet.Packet;

/**
 *
 * @author thronecth
 */
public class MultiServerHandler {

    public static void registerListener(Class<? extends Packet> clazz, PacketHandler listener) {
        if (!MultiServeClient.getConnectedListeners().containsKey(clazz)) {
            MultiServeClient.addListener(clazz, listener);
        } else {
            MDBase.getBase().log("You are trying to register a packet that already has a handler.");
        }
    }

}
