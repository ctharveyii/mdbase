/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.multiserver;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.mdbase.MDBase;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class PlayerIDHandler {

    private static final HashMap<String, PlayerID> cache = new HashMap();

    public static PlayerID getPlayerID(String name) throws NoIdException {
        if (cache.containsKey(name)) {
            return cache.get(name);
        }
        try {
            Player player = Bukkit.getPlayer(name);
            PlayerID id;
            if (player == null) {
                id = PlayerIDS.get(name, Bukkit.getOfflinePlayer(name).getUniqueId());
            } else {
                id = PlayerIDS.get(player);
            }

            if (id == null) {
                throw new NoIdException();
            }
            cache.put(name, id);
            return id;
        } catch (SQLException ex) {
            Logger.getLogger(PlayerIDHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static PlayerID getPlayerID(Player player) throws NoIdException {
        try {
            return PlayerIDS.get(player);
        } catch (SQLException ex) {
            Logger.getLogger(PlayerIDHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        throw new NoIdException();
    }

    public static class NoIdException extends Exception {

        public NoIdException() {
            super("Unable to locate player.  Make sure they have logged into our system at least once.");
        }

        public NoIdException(String string) {
            super(string);
        }

    }

}
