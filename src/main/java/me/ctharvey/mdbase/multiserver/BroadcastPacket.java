/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.multiserver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.mdbase.MDBase;
import me.ctharvey.multiserv.packet.JSONable;
import me.ctharvey.multiserv.packet.Packet;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.json.simple.JSONObject;

/**
 *
 * @author Charles
 */
public class BroadcastPacket extends Packet {

    public BroadcastPacket() {
    }

    public BroadcastPacket(String serverFrom, String serverTo, BroadcastPayloadInterface payload) {
        super(BroadcastPacket.class, serverFrom, serverTo, PacketType.GENERAL, payload);
    }

    @Override
    public BroadcastPayloadInterface getData() {
        return (BroadcastPayloadInterface) super.getStoredData();
    }

    @Override
    public void processData(JSONObject jo2) {
        try {
            String get = (String) jo2.get("class");
            BroadcastPayloadInterface payload = (BroadcastPayloadInterface) Class.forName(get).newInstance();
            payload.fromJSONObject(jo2);
            this.setData(payload);
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
            Logger.getLogger(BroadcastPacket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static class BroadcastPayloadFromPerm implements BroadcastPayloadInterface {

        private String permNode;
        private String message;
        private String prefix;

        public BroadcastPayloadFromPerm() {
        }

        public BroadcastPayloadFromPerm(String permNode, String message, String prefix) {
            this.permNode = permNode;
            this.message = ChatColor.translateAlternateColorCodes('&', message);
            this.prefix = prefix;
        }

        @Override
        public JSONObject toJSONObject() {
            JSONObject jo = new JSONObject();
            jo.put("class", this.getClass().getName());
            jo.put("permNode", this.permNode);
            jo.put("message", this.message);
            jo.put("prefix", this.prefix);
            return jo;
        }

        @Override
        public void fromJSONObject(JSONObject jo) {
            this.permNode = (String) jo.get("permnode");
            this.message = (String) jo.get("message");
            this.prefix = (String) jo.get("prefix");
        }

        @Override
        public List<Player> getValidPlayers() {
            List<Player> players = new ArrayList();
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.hasPermission(permNode) || permNode == null || "".equals(permNode)) {
                    players.add(player);
                }
            }
            return players;
        }

        @Override
        public void sendMessage() {
            Bukkit.getConsoleSender().sendMessage(getPrefix() + this.message);
            for (Player player : getValidPlayers()) {
                player.sendMessage(getPrefix() + message);
            }
        }

        @Override
        public String getPrefix() {
            return this.prefix;
        }

        @Override
        public void fromString(String string) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    public static class BroadcastPayLoadAll implements BroadcastPayloadInterface {

        private String msg;
        private String prefix;

        public BroadcastPayLoadAll(String msg, String prefix) {
            this.msg = ChatColor.translateAlternateColorCodes('&', msg);
            this.prefix = prefix;
        }

        public BroadcastPayLoadAll() {
        }

        @Override
        public List<Player> getValidPlayers() {
            return Arrays.asList(Bukkit.getOnlinePlayers());
        }

        @Override
        public void sendMessage() {
            Bukkit.getConsoleSender().sendMessage(getPrefix() + msg);
            for (Player player : getValidPlayers()) {
                player.sendMessage(getPrefix() + msg);
            }
        }

        @Override
        public JSONObject toJSONObject() {
            JSONObject jo = new JSONObject();
            jo.put("class", this.getClass().getName());
            jo.put("msg", this.msg);
            jo.put("prefix", this.prefix);
            return jo;
        }

        @Override
        public void fromJSONObject(JSONObject jo) {
            this.msg = (String) jo.get("msg");
            this.prefix = (String) jo.get("prefix");
        }

        @Override
        public String getPrefix() {
            return this.prefix;
        }

        @Override
        public void fromString(String string) {
        }

    }

    public static class BroadcastCertainUsersPayload implements BroadcastPayloadInterface {

        private List<String> playerNames;
        private String msg;
        private String prefix;
        private String permNode;

        public BroadcastCertainUsersPayload(String permnode, List<String> playerNames, String prefix, String msg) {
            this.permNode = permnode;
            this.playerNames = playerNames;
            this.prefix = prefix;
            this.msg = msg;
        }

        @Override
        public List<Player> getValidPlayers() {
            List<Player> targets = new ArrayList();
            for (Player player : Bukkit.getOnlinePlayers()) {
                for (String playerName : playerNames) {
                    if (player.getName().toLowerCase().equals(playerName.toLowerCase())) {
                        targets.add(player);
                    }
                }
            }
            return targets;
        }

        @Override
        public void sendMessage() {
            Bukkit.getConsoleSender().sendMessage(getPrefix() + msg);
            for (Player player : getValidPlayers()) {
                if (this.permNode == null || "".equals(this.permNode) || player.hasPermission(this.permNode)) {
                    player.sendMessage(getPrefix() + msg);
                }
            }
        }

        @Override
        public String getPrefix() {
            return this.prefix;
        }

        @Override
        public JSONObject toJSONObject() {
            JSONObject jo = new JSONObject();
            jo.put("permNode", this.permNode);
            jo.put("playerNames", this.playerNames);
            jo.put("msg", this.msg);
            jo.put("pefix", this.prefix);
            return jo;
        }

        @Override
        public void fromJSONObject(JSONObject jo) {
            this.playerNames = (List<String>) jo.get("playerNames");
            this.msg = (String) jo.get("msg");
            this.prefix = (String) jo.get("prefix");
            this.permNode = (String) jo.get("permNode");
        }

        @Override
        public void fromString(String string) {
        }

    }

}
