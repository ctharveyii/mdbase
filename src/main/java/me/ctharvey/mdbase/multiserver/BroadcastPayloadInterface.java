/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.multiserver;

import java.util.List;
import me.ctharvey.multiserv.packet.JSONable;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public interface BroadcastPayloadInterface extends JSONable {

    public List<Player> getValidPlayers();

    public void sendMessage();

    public String getPrefix();

}
