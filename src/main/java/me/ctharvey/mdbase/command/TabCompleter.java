/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.command;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class TabCompleter implements org.bukkit.command.TabCompleter {

    private final Command command;
    private static TabPlayerListInterface players;

    public TabCompleter(Command command) {
        this.command = command;
        players = new TabPlayerListInterface() {

            @Override
            public List<String> getValidPlayerNames() {
                List<String> players = new ArrayList();
                for (Player player : Bukkit.getOnlinePlayers()) {
                    players.add(player.getName());
                }
                return players;
            }
        };
    }

    public Command getCommand() {
        return command;
    }

    @Override
    public List<String> onTabComplete(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        List<String> values = new ArrayList();
        if (strings.length == 0) {
            if (command.getSubCommands().size() > 0) {
                for (SubCommand sCommand : command.getSubCommands()) {
                    values.add(sCommand.getName());
                }
            } else {
                return players.getValidPlayerNames();
            }
        }
        if (command.getSubCommands().size() > 0) {
            List<String> sCommands = new ArrayList();
            for (SubCommand sCommand : command.getSubCommands()) {
                sCommands.add(sCommand.getName());
            }
            for (String sCommandName : sCommands) {
                if (strings[0].length() < sCommandName.length() && sCommandName.substring(0, strings[0].length()).toLowerCase().equals(strings[0].toLowerCase())) {
                    values.add(sCommandName);
                }
            }
        } else {
            for (String playerName : players.getValidPlayerNames()) {
                if (strings[0].length() < playerName.length() && playerName.substring(0, strings[0].length()).toLowerCase().equals(strings[0].toLowerCase())) {
                    values.add(playerName);
                }
            }
        }
        return values;
    }

    public static interface TabPlayerListInterface {

        public List<String> getValidPlayerNames();

    }

    public static void setPlayers(TabPlayerListInterface players) {
        TabCompleter.players = players;
    }

}
