/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import me.ctharvey.pluginbase.MDBasePlugin;
import net.minecraft.util.org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public abstract class Command implements MDBasePlugin.CommandInterface {

    private final String name;

    private final MDBasePlugin plugin;

    private final Set<SubCommand> subCommands = new HashSet();

    protected TabCompleter tabCompleter;

    public Command(MDBasePlugin plugin, String name) {
        this.name = name;
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        if (strings != null && strings.length == 0) {
            Set<SubCommand> commands = getSubCommandsPlayerHasPermsFor(cs);
            if (commands.size() > 0) {
                plugin.sendMsg("You have did not provide a valid subcommand.", cs);
                List<String> commandNames = getSubCommandNames(commands);
                plugin.sendMsg("Valid SubCommands: " + StringUtils.join(commandNames, " "), cs);
            } else {
                plugin.sendMsg("You don't have access to any subcommands", cs);
            }
        } else {
            SubCommand subCommand = determineSubCommand(strings[0]);
            if (subCommand == null && subCommands.size() > 0) {
                Set<SubCommand> commands = getSubCommandsPlayerHasPermsFor(cs);
                if (commands.size() > 0) {
                    plugin.sendMsg("You have did not provide a valid subcommand.", cs);
                    List<String> commandNames = getSubCommandNames(commands);
                    plugin.sendMsg("Valid SubCommands: " + StringUtils.join(commandNames, " "), cs);
                } else {
                    plugin.sendMsg("You don't have access to any subcommands", cs);
                }
            } else if (subCommand != null) {
                subCommand.execute(cs, strings);
            }
        }
        return true;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<String> getCommandNames() {
        return Arrays.asList(name);
    }

    public Set<SubCommand> getSubCommands() {
        return subCommands;
    }

    public void addSubCommand(SubCommand sc) {
        this.subCommands.add(sc);
    }

    public MDBasePlugin getPlugin() {
        return plugin;
    }

    private SubCommand determineSubCommand(String sbName) {
        for (SubCommand sb : subCommands) {
            if (sb.getName().toLowerCase().equals(sbName.toLowerCase())) {
                return sb;
            }
        }
        return null;
    }

    private Set<SubCommand> getSubCommandsPlayerHasPermsFor(CommandSender cs) {
        Set<SubCommand> commands = new HashSet();
        for (SubCommand sCommand : this.subCommands) {
            if (cs.hasPermission(sCommand.getPermnode())) {
                commands.add(sCommand);
            }
        }
        return commands;
    }

    private List<String> getSubCommandNames(Set<SubCommand> commands) {
        List<String> list = new ArrayList();
        for (SubCommand command : commands) {
            list.add(command.getName());
        }
        return list;
    }

    public TabCompleter getTabCompleter() {
        return tabCompleter;
    }

    
    


}
