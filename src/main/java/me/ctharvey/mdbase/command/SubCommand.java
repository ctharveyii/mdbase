/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.command;

import java.util.Arrays;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public abstract class SubCommand {

    private final SubCommandType type;

    private final String name, permnode;

    private final Command command;

    public SubCommand(Command command, String name, SubCommandType type, String permnode) {
        this.name = name;
        this.type = type;
        this.command = command;
        this.permnode = permnode;
    }

    public final void execute(CommandSender cs, String[] arguments) {
        arguments = Arrays.copyOfRange(arguments, 1, arguments.length);
        if (cs.isOp() || cs.hasPermission(permnode)) {
            if (cs instanceof ConsoleCommandSender && type != SubCommandType.PLAYER) {
                runSubCommand(cs, arguments);
            } else if (cs instanceof ConsoleCommandSender) {
                command.getPlugin().sendMsg("This can only be executed by a player.", cs);
            }
        }
        if (cs instanceof Player && type != SubCommandType.CONSOLE) {
            runSubCommand(cs, arguments);
        } else if (cs instanceof Player) {
            command.getPlugin().sendMsg("This can only be executed by the console.", cs);
        } else {
            command.getPlugin().sendMsg("You do not have permission for this command.", cs);
        }
    }

    protected abstract void runSubCommand(CommandSender cs, String[] args);

    public String getName() {
        return name;
    }

    public static enum SubCommandType {

        CONSOLE,
        PLAYER,
        ALL;
    }

    public String getPermnode() {
        return permnode;
    }

    public Command getCommand() {
        return command;
    }

    public SubCommandType getType() {
        return type;
    }

}
