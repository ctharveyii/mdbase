/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.ctharvey.mdbase.enums.list;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thronecth
 */
public class ListHandler {
    
    public static List<String> toString(List list){
        List<String> vals = new ArrayList();
        for(Object obj: list){
            vals.add(obj.toString());
        }
        return vals;
    }
    
}
