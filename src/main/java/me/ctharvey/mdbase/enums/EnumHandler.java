/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.ctharvey.mdbase.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import me.ctharvey.multiserv.packet.Packet;

/**
 *
 * @author thronecth
 */
public class EnumHandler {
    
    public static <T extends Enum<T>>  List<String> getKeys(Class<T> enumerator){
        EnumSet<T> allOf = EnumSet.allOf(enumerator);
        List<String> names = new ArrayList();
        for(T enumVal: allOf){
            names.add(enumVal.name());
        }
        return names;
    }

    public static <T extends Enum<T>> List<Integer> toOrdinalList(Class<T> enumerator) {
        EnumSet<T> allOf = EnumSet.allOf(enumerator);
        List<Integer> ordinals = new ArrayList();
        for(T enumVal: allOf){
            ordinals.add(enumVal.ordinal());
        }
        return ordinals;
    }
    
    public static <T extends Enum<T>> List<Integer> toOrdinalList(List<T> enums) {
        List<Integer> ordinals = new ArrayList();
        for(T enumVal: enums){
            ordinals.add(enumVal.ordinal());
        }
        return ordinals;
    }
    
}
