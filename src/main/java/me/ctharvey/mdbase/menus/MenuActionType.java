/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.menus;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public enum MenuActionType {

    CANCEL(Material.ANVIL),
    GOBACK(Material.COMPASS),
    COMPLETE(Material.ANVIL),
    OTHER;

    private Material itemThatRepresentsAction;

    private MenuActionType(Material itemThatRepresentsAction) {
        this.itemThatRepresentsAction = itemThatRepresentsAction;
    }

    public Material getItemThatRepresentsAction() {
        return itemThatRepresentsAction;
    }

    private MenuActionType() {
    }

    public static MenuActionType getTypeFromItem(ItemStack currentItem) {
        for (MenuActionType type : values()) {
            if (type.getItemThatRepresentsAction().equals(currentItem.getType())) {
                if (currentItem.hasItemMeta() && currentItem.getItemMeta().hasDisplayName()) {
                    String stripColor = ChatColor.stripColor(currentItem.getItemMeta().getDisplayName().toLowerCase());
                    if (stripColor.contains(type.name())) {
                        return type;
                    }
                }
            }
        }
        return OTHER;
    }

}
