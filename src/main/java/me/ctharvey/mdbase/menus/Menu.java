/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.menus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import me.ctharvey.mdbase.menus.MenuButton.MenuButtonType;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public class Menu extends com.adamantdreamer.foundation.ui.menu.Menu {

    final int buttonCapacity = 56;
    
    int page = 1;
    protected Set<MenuButtonType> specialButtonsToUse = new HashSet();
    protected List<MenuButton> buttons = new ArrayList(buttonCapacity);
    protected List<MenuItem> items = new ArrayList();

    public Menu(String title, MenuHandler handler) {
        super(title, handler);
    }

    public void set(int slot, ItemStack item, String name, List<String> lore) {
        if (item.getType() == Material.AIR) {
            return;
        }
        this.items.set(slot, new MenuItem(name, item.getType(), 1, lore));
    }

    public void set(int slot, Material mat, String name, List<String> lore) {
        if (mat == Material.AIR) {
            return;
        }
        this.items.set(slot, new MenuItem(name, mat, 1, lore));
    }

    public void set(int slot, Material mat, String name, String... lore) {
        if (mat == Material.AIR) {
            return;
        }
        this.items.set(slot, new MenuItem(name, mat, 1, Arrays.asList(lore)));
    }

    @Override
    public void show(Player player) {
        processNormalButtons();
        processSpecialButtons();
        for(MenuButton button: buttons){
            super.add(new ItemStack(button.getMenuItem().getMatType()), button.getMenuItem().getName(), button.getMenuItem().getLore());
        }
        super.show(player); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Menu add(ItemStack icon, String displayName, String... lore) {
        this.items.add(new MenuItem(displayName, icon.getType(), icon.getAmount(), lore));
        return this;
    }

    public Menu add(String name, Material matType, int numberOf, List<String> lore) {
        this.items.add(new MenuItem(name, matType, numberOf, lore));
        return this;
    }

    private void processSpecialButtons() {
        for (MenuButtonType type : this.specialButtonsToUse) {
            type.addButtonToMenu(this);
        }
    }

    private void processNormalButtons() {
        int sizeSpecial = this.specialButtonsToUse.size();
        int space = this.buttonCapacity - sizeSpecial;
        int startSlot = (space * page) - space;
        int cnt = 0;
        for(int x = startSlot; x < space * page && x < this.items.size(); x++){
            this.buttons.add(cnt, new MenuButton(this.items.get(x), MenuButtonType.OTHER, cnt));
            cnt++;
        }
    }

    public List<MenuButton> getButtons() {
        return buttons;
    }
    
    public void useCancelButton() {
        this.specialButtonsToUse.add(MenuButtonType.CANCEL);
    }
    
    public void useCompleteButton() {
        this.specialButtonsToUse.add(MenuButtonType.COMPLETE);
    }

}
