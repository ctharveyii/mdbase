/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.menus;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public abstract class MenuHandler implements com.adamantdreamer.foundation.ui.menu.MenuHandler {

    protected final CommandSender cs;
    protected Object object;
    protected int id;
    private Menu menu;
    private final MenuCompleteInterface completeButtonPushed;
    protected MenuButton buttonLastPushed;

    public MenuHandler(CommandSender cs, Object object, MenuCompleteInterface completeButtonPushed) {
        this.cs = cs;
        this.object = object;
        this.completeButtonPushed = completeButtonPushed;
    }

    @Override
    public Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
        id = ice.getRawSlot();
        if (id < 0) {
            ice.getView().close();
            return Result.DENY;
        }
        if (menu != null && ice.getRawSlot() < menu.getButtons().size()) {
            MenuButton get = menu.getButtons().get(ice.getRawSlot());
            switch (get.getType()) {
                case CANCEL:
                    ice.getView().close();
                    ice.setCancelled(true);
                    break;
                case GOBACK:
                    ice.getView().close();
                    ice.setCancelled(true);
                    menu.show((Player) cs);
                    break;
                case COMPLETE:
                    ice.getView().close();
                    ice.setCancelled(true);
                    this.completeButtonPushed.complete();
            }
        }
        return Result.DEFAULT;
    }

    @Override
    public Event.Result onDrop(PlayerDropItemEvent pdie, boolean bln) {
        return Result.DENY;
    }

    @Override
    public void onClose(InventoryCloseEvent ice) {
        object = null;
        ice.getView().close();
    }

    public static interface MenuCompleteInterface {

        public void complete();

    }

    public static interface MenuGoBackInterface {

        public void goBack();

    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Menu getMenu() {
        return menu;
    }
    
}
