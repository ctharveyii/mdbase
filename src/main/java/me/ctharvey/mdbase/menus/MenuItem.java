/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.menus;

import java.util.List;
import org.bukkit.Material;

/**
 *
 * @author thronecth
 */
public class MenuItem {

    private final String name;
    private final Material matType;
    private int numberOf = 1;
    private String[] lore = new String[]{};

    public MenuItem(String name, Material matType, int numberOf, List<String> lore) {
        this.name = name;
        this.matType = matType;
        this.numberOf = numberOf;
        this.lore = lore.toArray(new String[0]);
    }

    public MenuItem(String displayName, Material type, int amount, String[] lore) {
        this.name = displayName;
        this.matType = type;
        this.numberOf = amount;
        this.lore = lore;
    }

    public String getName() {
        return name;
    }

    public Material getMatType() {
        return matType;
    }

    public int getNumberOf() {
        return numberOf;
    }

    public String[] getLore() {
        return lore;
    }

}
