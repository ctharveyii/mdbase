/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.menus;

import java.util.Arrays;
import org.bukkit.Material;

/**
 *
 * @author thronecth
 */
public class MenuButton {

    private MenuItem menuItem;
    private MenuButtonType type;
    int slot;

    public MenuButton(MenuItem menuItem, MenuButtonType type, int slot) {
        this.menuItem = menuItem;
        this.type = type;
        this.slot = slot;
    }

    public static enum MenuButtonType {
        CANCEL,
        GOBACK,
        COMPLETE,
        OTHER;

        public void addButtonToMenu(Menu menu) {
            switch(this){
                case CANCEL:
                    menu.getButtons().add(new MenuButton(new MenuItem("Cancel", Material.ANVIL, 1, Arrays.asList("Cancel")), this, menu.getButtons().size()));
                    break;
                case COMPLETE:
                    menu.getButtons().add(new MenuButton(new MenuItem("Complete", Material.BEACON, 1, Arrays.asList("Complete")), this, menu.getButtons().size()));
                    break;
                case GOBACK:
                    menu.getButtons().add(new MenuButton(new MenuItem("Go back", Material.LAVA, 1, Arrays.asList("Go back")), this, menu.getButtons().size()));
                    break;
                case OTHER:
                    break;
            }
        }
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public MenuButtonType getType() {
        return type;
    }

    public int getSlot() {
        return slot;
    }
    
    

}
