/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.chat;

import java.util.List;
import me.ctharvey.mdbase.MDBase;
import me.ctharvey.mdbase.multiserver.BroadcastPacket;
import me.ctharvey.mdbase.multiserver.BroadcastPayloadInterface;
import me.ctharvey.pluginbase.MDBasePlugin;
import me.ctharvey.pluginbase.MessageSender;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author thronecth
 */
public class LocalSend implements MessageSender<BroadcastPayloadInterface> {

    private MDBasePlugin plugin;

    @Override
    public void sendMsg(CommandSender cs, Object text) {
        String msg;
        if (text instanceof String) {
            msg = (String) text;
        } else if (text instanceof Integer) {
            msg = (Integer.toString((Integer) text));
        } else {
            msg = text.toString();
        }
        text = ChatColor.translateAlternateColorCodes('&', msg);
        cs.sendMessage(ChatColor.RESET + getPlugin().getPrefix() + text);
    }

    @Override
    public void log(Object msg) {
        if (msg instanceof String) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', getPlugin().getPrefix() + msg));
        } else if (msg instanceof Integer) {
            Bukkit.getConsoleSender().sendMessage(getPlugin().getPrefix() + Integer.toString((Integer) msg));
        } else {
            msg = new ReflectionToStringBuilder(msg);
            Bukkit.getConsoleSender().sendMessage(getPlugin().getPrefix() + msg);
        }
    }

    @Override
    public void logError(Object msg) {
        if (msg instanceof String) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', getPlugin().getPrefix() + "&4ERROR: " + msg));
        } else if (msg instanceof Integer) {
            Bukkit.getConsoleSender().sendMessage(getPlugin().getPrefix() + Integer.toString((Integer) msg));
        } else {
            msg = new ReflectionToStringBuilder(msg);
            Bukkit.getConsoleSender().sendMessage(getPlugin().getPrefix() + ChatColor.RED + "ERROR: " + msg);
        }
    }

    @Override
    public void broadcast(String msg) {
        BroadcastPacket.BroadcastPayLoadAll payload = new BroadcastPacket.BroadcastPayLoadAll(msg, getPlugin().getPrefix());
        payload.sendMessage();
    }

    @Override
    public void broadcast(String permnode, String msg) {
        BroadcastPacket.BroadcastPayloadFromPerm payload = new BroadcastPacket.BroadcastPayloadFromPerm(permnode, msg, getPlugin().getPrefix());
        payload.sendMessage();
    }

    @Override
    public void broadcast(List<String> playerNames, String permnode, String msg) {
        BroadcastPacket.BroadcastCertainUsersPayload payload = new BroadcastPacket.BroadcastCertainUsersPayload(permnode, playerNames, getPlugin().getPrefix(), msg);
        payload.sendMessage();
    }

    @Override
    public MDBasePlugin getPlugin() {
        if(plugin == null){
            return MDBase.getBase();
        }
        return plugin;
    }

    public void setPlugin(MDBase plugin) {
        this.plugin = plugin;
    }

    @Override
    public <K extends MDBasePlugin> void setPlugin(K plugin) {
        this.plugin = plugin;
    }

}
