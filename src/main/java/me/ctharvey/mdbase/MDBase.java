/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase;

import me.ctharvey.dataobjects.SerializableLocation;
import me.ctharvey.mdbase.chat.LocalSend;
import me.ctharvey.mdbase.multiserver.BroadcastPacket;
import me.ctharvey.mdbase.teleport.DefaultTeleporter;
import me.ctharvey.mdbase.vault.VaultManager;
import me.ctharvey.multiserv.MultiServe;
import me.ctharvey.multiserv.client.MultiServeClient;
import me.ctharvey.multiserv.client.PacketHandler;
import me.ctharvey.multiserv.packet.Packet;
import me.ctharvey.pluginbase.MDBasePlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.serialization.ConfigurationSerialization;

/**
 *
 * @author thronecth
 */
public class MDBase extends MDBasePlugin {

    private static MDBase base;
    private me.ctharvey.mdbase.teleport.TeleporterInterface teleporter;

    public MDBase() {
        super("MDBase", ChatColor.YELLOW, "[", "]", new LocalSend());
        ConfigurationSerialization.registerClass(SerializableLocation.class);
    }

    @Override
    public void startup() {
        MDBase.base = this;
        VaultManager.init();
    }

    @Override
    public void onDisable() {
        super.onDisable(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(new MDBaseListener(), this);
        MultiServeClient.addListener(BroadcastPacket.class, new BroadcastPacketHandler());
        super.onEnable(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void shutdown() {
    }

    public static MDBase getBase() {
        return base;
    }

    public class BroadcastPacketHandler extends PacketHandler<BroadcastPacket>{

        public BroadcastPacketHandler() {
            super("BroadcastHandler");
        }

        @Override
        public void run(BroadcastPacket packet) {
            packet.getData().sendMessage();
        }
        
    }
}
