/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.logger;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.dataobjects.jsonable.JSONableList;
import me.ctharvey.mdbase.MDBase;
import me.ctharvey.multiserv.packet.JSONable;

/**
 *
 * @author thronecth
 */
public class LogHandler {

    public static JSONableList<JSONable.JSONableString> getLogsFromFile(PlayerID id) {
        JSONableList<JSONable.JSONableString> lines = new JSONableList<>(JSONable.JSONableString.class);
        File file = new File("./server.log");
        try {
            Process p = Runtime.getRuntime().exec(new String[]{"bash", "-c", "wc -l " + file.getAbsolutePath()});
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String firstWord = br.readLine().split(" ")[0];
//			Helper.styleLog(firstWord);
            int lineCount = Integer.valueOf(firstWord);
            for (int x = 0; x < lineCount; x++) {
                String line = in.readLine();
                if (x > lineCount - 500 && line.contains(id.getName()) && lines.size() < 100) {
                    lines.add(new JSONable.JSONableString(line));
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LogHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lines;
    }
}
