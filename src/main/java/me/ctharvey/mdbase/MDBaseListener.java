/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase;

import java.util.List;
import me.ctharvey.multiserv.ServerConnectEvent;
import me.ctharvey.pluginbase.MDBasePlugin;
import me.ctharvey.pluginbase.MessageSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

/**
 *
 * @author thronecth
 */
public class MDBaseListener implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onServerConnect(ServerConnectEvent event) {
        List<MDBasePlugin> plugins = MDBasePlugin.getPlugins();
        for (MDBasePlugin plugin : plugins) {
            MessageSender multiSender = plugin.getMultiSender();
            if (multiSender != null) {
                plugin.setCurrentMessenger(multiSender);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onServerDisconnect(ServerConnectEvent.ServerDisconnectEvent event) {
        List<MDBasePlugin> plugins = MDBasePlugin.getPlugins();
        for (MDBasePlugin plugin : plugins) {
            MessageSender localSender = plugin.getLocalSender();
            if (localSender != null) {
                plugin.setCurrentMessenger(localSender);
            }
        }
    }

}
