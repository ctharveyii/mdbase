/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.vault;

import java.util.logging.Level;
import me.ctharvey.mdbase.MDBase;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

/**
 *
 * @author thronecth
 */
public class VaultManager {

    private static Permission permHandler = null;
    private static Chat chatHandler = null;
    private static Economy econHandler = null;
    
    public static void init() {
        boolean setupPerm = setupPerm();
        boolean setupChat = setupChat();
        boolean setupECon = setupEcon();
        if (setupPerm) {
            MDBase.getBase().log("Vault perm hook succeeded.");
        } else
            MDBase.getBase().logError("Vault perm hook failed.");
        if (setupChat) {
            MDBase.getBase().log("Vault chat hook succeeded.");
        } else {
            MDBase.getBase().logError("Vault chat hook failed.");
        }
        if (setupECon) {
            MDBase.getBase().log("Vault econ hook succeeded.");
        } else {
            MDBase.getBase().logError("Vault econ hook failed.");
        }
    }

    private static boolean setupEcon() {
        if (Bukkit.getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econHandler = rsp.getProvider();
        return econHandler != null;
    }

    public static boolean setupPerm() {
        RegisteredServiceProvider<Permission> permissionProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permHandler = permissionProvider.getProvider();
        }
        return (permHandler != null);
    }

    private static boolean setupChat() {
        RegisteredServiceProvider<Chat> chatProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chatHandler = chatProvider.getProvider();
        }

        return (chatHandler != null);
    }

    public static Permission getPermHandler() {
        return permHandler;
    }

    public static Chat getChatHandler() {
        return chatHandler;
    }

    public static Economy getEconHandler() {
        return econHandler;
    }
    
}
