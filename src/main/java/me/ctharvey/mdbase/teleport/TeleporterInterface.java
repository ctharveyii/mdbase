/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.teleport;

import com.adamantdreamer.foundation.core.db.PlayerID;
import me.ctharvey.dataobjects.SerializableLocation;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public interface TeleporterInterface {

    public void teleportPlayerToPlayer(PlayerID user, PlayerID user1) throws PlayerIDHandler.NoIdException, InvalidCrossServerTP;

    public void teleportPlayerToLocation(PlayerID player, SerializableLocation location, String serverName) throws InvalidCrossServerTP;

    public void teleportPlayerToServer(PlayerID player, String serverName) throws InvalidCrossServerTP;

    public class InvalidCrossServerTP extends Exception {

        public InvalidCrossServerTP(String string) {
            super(string);
        }

    }

}
