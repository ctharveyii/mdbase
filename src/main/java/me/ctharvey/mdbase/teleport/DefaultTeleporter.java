/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.teleport;

import com.adamantdreamer.foundation.core.db.PlayerID;
import me.ctharvey.dataobjects.SerializableLocation;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class DefaultTeleporter implements TeleporterInterface {
    
    @Override
    public void teleportPlayerToPlayer(PlayerID user, PlayerID user1) throws PlayerIDHandler.NoIdException, InvalidCrossServerTP {
        Player player1 = Bukkit.getPlayer(user.getUUID());
        Player player2 = Bukkit.getPlayer(user1.getUUID());
        if (player1 == null || player2 == null) {
            throw new InvalidCrossServerTP("One or the other player is a null object.");
        } else {
            player1.teleport(player2);
        }
    }
    
    @Override
    public void teleportPlayerToLocation(PlayerID player, SerializableLocation location, String servername) throws InvalidCrossServerTP {
        Player player1 = Bukkit.getPlayer(player.getUUID());
        if (player1 == null || !Bukkit.getServerName().equals(servername)) {
            throw new InvalidCrossServerTP("Player for tp to location is null.");
        } else {
            Location location1 = location.getLocation();
            if (location1 == null) {
                throw new InvalidCrossServerTP("Location needed is not on this server and multiserve is not supported.");
            } else {
                player1.teleport(location1);
            }
        }
    }
    
    @Override
    public void teleportPlayerToServer(PlayerID player, String serverName) throws InvalidCrossServerTP {
        throw new InvalidCrossServerTP("Unable to tp to different servers as multiserve system is down.");
    }
    
}
