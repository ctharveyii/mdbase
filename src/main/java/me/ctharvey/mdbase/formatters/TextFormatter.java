/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.formatters;

/**
 *
 * @author thronecth
 */
public class TextFormatter {

    public static String join(String[] arguments, int startPos) {
        if (arguments.length > startPos) {
            int adjust = 0;
            if (startPos != 0) {
                adjust = 1;
            }
            StringBuilder sb = new StringBuilder("");
            for (int i = startPos; i < arguments.length - startPos + adjust; i++) {
                sb.append(arguments[i]);
                if (i != (arguments.length - startPos)) {
                    sb.append(" ");
                }
            }
            return sb.toString();
        } else {
            // There was nothing to join, return an empty string.
            return "";
        }
    }
    
    public static String join(String[] a, String seperator, int start) {
        StringBuilder result = new StringBuilder();
        if (a.length > start) {
            result.append(a[start]);
            for (int i = start + 1; i < a.length; i++) {
                if (a[i -1] == null || !a[i - 1].equals("")) {
                    result.append(seperator);
                }
                result.append(a[i]);
            }
        }
        return result.toString();
    }
}
