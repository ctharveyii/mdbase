/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.formatters;

/**
 *
 * @author thronecth
 */
public class TimeFormatter {

    public static String formatTime(int until) {
        int seconds = until;
        int minutes = seconds / 60;
        int hours = minutes / 60;
        int days = hours / 24;
        if (minutes != 0) {
            if (hours != 0) {
                if (days != 0) {
                    return days + " days";
                }
                return hours + " hours";
            }
            return minutes + " minutes";
        }
        return seconds + " seconds";
    }

    public static String formatTime(long until) {
        long seconds = until;
        long minutes = seconds / 60L;
        long hours = minutes / 60L;
        long days = hours / 24L;
        if (minutes != 0) {
            if (hours != 0) {
                if (days != 0) {
                    return days + " days";
                }
                return hours + " hours";
            }
            return minutes + " minutes";
        }
        return seconds + " seconds";
    }
}
