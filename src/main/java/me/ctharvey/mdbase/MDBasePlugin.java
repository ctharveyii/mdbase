/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase;

import me.ctharvey.mdbase.chat.MultiSend;
import me.ctharvey.mdbase.teleport.DefaultTeleporter;
import me.ctharvey.mdbase.teleport.TeleporterInterface;
import me.ctharvey.pluginbase.MessageSender;
import org.bukkit.ChatColor;

/**
 *
 * @author thronecth
 */
public abstract class MDBasePlugin extends me.ctharvey.pluginbase.MDBasePlugin {

    private TeleporterInterface currentTeleporter;
    private TeleporterInterface multiTeleporter;
    private TeleporterInterface localTeleporter;

    public MDBasePlugin(String name, ChatColor color, String openingDecor, String closignDecor, MessageSender localSender) {
        super(name, color, openingDecor, closignDecor, localSender);
        this.setMultiSender(new MultiSend(this));
        this.currentTeleporter = new DefaultTeleporter();
        localTeleporter = this.currentTeleporter;
    }

    public MDBasePlugin(String name, ChatColor color, String openingDecor, String closignDecor, MessageSender localSender, TeleporterInterface teleporter) {
        super(name, color, openingDecor, closignDecor, localSender);
        this.setMultiSender(new MultiSend(this));
        this.currentTeleporter = teleporter;
        localTeleporter = teleporter;
    }

    public void setCurrentTeleporter(TeleporterInterface currentTeleporter) {
        this.currentTeleporter = currentTeleporter;
    }

    public void setMultiTeleporter(TeleporterInterface multiTeleporter) {
        this.multiTeleporter = multiTeleporter;
    }

    public void setLocalTeleporter(TeleporterInterface localTeleporter) {
        this.localTeleporter = localTeleporter;
    }

    public TeleporterInterface getCurrentTeleporter() {
        return currentTeleporter;
    }

    public TeleporterInterface getMultiTeleporter() {
        return multiTeleporter;
    }

    public TeleporterInterface getLocalTeleporter() {
        return localTeleporter;
    }
    
}
