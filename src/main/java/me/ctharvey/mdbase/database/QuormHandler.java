/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.database;

import com.adamantdreamer.foundation.core.DatabaseConfig;
import com.adamantdreamer.quorm.core.DAO;
import com.adamantdreamer.quorm.core.Link;
import com.adamantdreamer.quorm.core.Schema;
import com.adamantdreamer.quorm.exception.ColumnMismatchException;
import com.adamantdreamer.quorm.link.mysql.MysqlLink;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.mdbase.MDBase;
import me.ctharvey.pluginbase.MDBasePlugin;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author thronecth
 */
public class QuormHandler {

    private Link primaryLink;
    private DAO dao;
    private DatabaseConfig config;
    private MDBasePlugin plugin;
    protected boolean dbConnected = false;

    public QuormHandler(MDBasePlugin plugin, DatabaseConfig config) {
        String str;
        this.plugin = plugin;
        this.config = config;
        switch ((str = config.type.toLowerCase()).hashCode()) {
            case -894935028:
                if (str.equals("sqlite")) {
                    break;
                }
            case 104382626:
                if ((str.equals("mysql"))) {
                    primaryLink = new MysqlLink(config.host, config.port, config.user, config.pass);
                    try {
                        primaryLink.connect();
                        plugin.log("Connected to MySQL Server");
                        dbConnected = true;
                    } catch (Exception ex) {
                        plugin.logError("Error connecting to MySQL Server " + ex.toString());
                    }
                }
                break;
            default:
                plugin.logError("Unrecongized connection type \"" + plugin.getConfig().getString("type") + "\", no database link enabled.");
                plugin.getPluginLoader().disablePlugin(plugin);
                Bukkit.getServer().shutdown();
                return;
        }
        if (primaryLink != null) {
            dao = primaryLink.getDAO();
            if (dao == null) {
                MDBase.getBase().logError("DAO is null!");
                Bukkit.getServer().shutdown();
            }
        } else {
            MDBase.getBase().logError("Primary Link is null!");
            Bukkit.getServer().shutdown();
        }
    }

    public void addSchema(Class clazz) {
        Schema schema = Schema.get(config.primarySchema);
        schema.addTables(new Class[]{clazz});
        try {
            dao.build(schema);
        } catch (SQLException | ColumnMismatchException ex) {
            Logger.getLogger(QuormHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Link getPrimaryLink() {
        return primaryLink;
    }

    public DAO getDao() {
        return dao;
    }

    public MDBasePlugin getPlugin() {
        return plugin;
    }

    public boolean isDbConnected() {
        return dbConnected;
    }

}
