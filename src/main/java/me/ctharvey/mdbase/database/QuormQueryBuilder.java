/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase.database;

import com.adamantdreamer.quorm.query.PreparedQuery;
import com.adamantdreamer.quorm.query.Query;

/**
 *
 * @author thronecth
 */
public class QuormQueryBuilder {

    public static String generatePlaceHolders(int length) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length;) {
            builder.append("?");
            if (++i < length) {
                builder.append(",");
            }
        }
        return builder.toString();
    }

}
