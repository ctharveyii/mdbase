/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.mdbase;

import com.adamantdreamer.foundation.core.DatabaseConfig;
import com.adamantdreamer.foundation.core.Path;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.mdbase.database.QuormHandler;
import me.ctharvey.pluginbase.MessageSender;
import org.bukkit.ChatColor;

/**
 *
 * @author thronecth
 */
public abstract class MDDatabasePlugin extends MDBasePlugin {

    protected QuormHandler dbHandler;

    public MDDatabasePlugin(String name, ChatColor color, String openingDecor, String closignDecor, MessageSender log) {
        super(name, color, openingDecor, closignDecor, log);
    }

    @Override
    public void onEnable() {
        try {
            initDB();
            super.onEnable();
        } catch (NullPointerException e) {
            dbHandler.getPlugin().logError("NPE reached at:");
            for (StackTraceElement ele : e.getStackTrace()) {
                dbHandler.getPlugin().logError(ele.toString());
            }
        }

    }

    protected void initDB() {
        try {
            DatabaseConfig config;
            saveDefaultConfig();
            getConfig().options().copyDefaults(true);
            getConfig().getString("type");
            config = (DatabaseConfig) loadConfig(new DatabaseConfig());
            this.log("<Mysql Connection> user: " + config.user + " db: " + config.primarySchema + " type: " + config.type);
            dbHandler = new QuormHandler(this, config);
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(MDBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public QuormHandler getDbHandler() {
        return dbHandler;
    }

    public void loadConfig(Object... intoObjects) throws IllegalArgumentException, IllegalAccessException {
        for (Object intoObject : intoObjects) {
            loadConfig(intoObject);
        }
    }

    public <T> T loadConfig(T intoObject) throws IllegalArgumentException, IllegalAccessException {
        String basePath = getPath(intoObject.getClass());
        for (Field field : intoObject.getClass().getDeclaredFields()) {
            String path = basePath + getPath(field);
            if (getConfig().contains(path)) {
                field.setAccessible(true);
                switch (field.getType().getSimpleName().toLowerCase()) {
                    case "string":
                        field.set(intoObject, getConfig().getString(path));
                        break;
                    case "int":
                    case "integer":
                        field.set(intoObject, getConfig().getInt(path));
                        break;
                }

            }
        }
        return intoObject;
    }

    public void saveConfig(Object... fromObjects) throws IllegalArgumentException, IllegalAccessException {
        for (Object fromObject : fromObjects) {
            saveConfig(fromObject);
        }
    }

    public <T> T saveConfig(T fromObject) throws IllegalArgumentException, IllegalAccessException {
        String basePath = getPath(fromObject.getClass());
        for (Field field : fromObject.getClass().getDeclaredFields()) {
            String path = basePath + getPath(field);
            field.setAccessible(true);
            getConfig().set(path, field.get(fromObject));
        }
        return fromObject;
    }

    private String getPath(Class<?> clazz) {
        return (clazz.isAnnotationPresent(Path.class)
                ? clazz.getAnnotation(Path.class).value() + "."
                : "");
    }

    private String getPath(Field field) {
        return (field.isAnnotationPresent(Path.class)
                ? field.getAnnotation(Path.class).value()
                : field.getName());
    }

    public void addSchema(Class clazz) {
        dbHandler.addSchema(clazz);
    }

}
