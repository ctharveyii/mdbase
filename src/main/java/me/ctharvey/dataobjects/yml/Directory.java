/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.dataobjects.yml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import me.ctharvey.mdbase.MDBase;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author charles
 */
/**
 * Represents a directory on a file system.
 */
public class Directory {

    private final String rootLoc, location;
    private final File file;
    public List<Directory> subDirs;

    public Directory(String location, Plugin plugin) {
        this.location = location;
        this.rootLoc = plugin.getDataFolder() + File.separator;
        if (!new File(rootLoc).exists()) {
            new File(rootLoc).mkdir();
        }
        file = new File(rootLoc + location);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    public Directory(String rLoc, String location) {
        if (!rLoc.substring(rLoc.length()).equals(File.separator)) {
            rLoc = rLoc + File.separator;
        }
        this.location = location;
        this.rootLoc = rLoc;
        if (!new File(rootLoc).exists()) {
            new File(rootLoc).mkdir();
        }
        file = new File(rootLoc + location);

        if (!file.exists()) {
            file.mkdir();
        }
    }

    public void addSubDirectories(List<String> locations) {
        for (String loc : locations) {
            Directory dir = new Directory(rootLoc, location + File.separator + loc);
        }
    }

    public List<File> getDirectoriesIn() {
        List<File> dirs = new ArrayList();
        for (File fl : file.listFiles()) {
            if (fl.isDirectory()) {
                dirs.add(fl);
            }
        }
        return dirs;
    }

    public List<File> getFilesIn() {
        List<File> files = new ArrayList();
        for (File fl : file.listFiles()) {
            MDBase.getBase().log(fl.getName());
            if (fl.isFile()) {
                files.add(fl);
            }
        }
        return files;
    }

    public String getLocation() {
        return location;
    }

    public String getRootLocation() {
        return rootLoc;
    }

    public File getFile() {
        return file;
    }

}
