/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.dataobjects.yml;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.mdbase.MDBase;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author charles
 */
public final class YMLfiles {
    
    private final String filename;
    private final File file;
    private YamlConfiguration config = null;
    private final Plugin plugin;

    /**
     * Create a new YML file with the name and the config options.
     *
     * @param filename The name of the YML file.
     * @param plugin
     * @param configOpt The config options to use.
     */
    public YMLfiles(String filename, Plugin plugin) {
        this.filename = filename;
        this.plugin = plugin;
        file = new File(plugin.getDataFolder() + File.separator + filename);
        try {
            load();
        } catch (IOException ex) {
            Logger.getLogger(YMLfiles.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public YMLfiles(String root, String location, Plugin plugin) {
        MDBase.getBase().log("DERP::::::" + root);
        this.plugin = plugin;
        String endChar = root.substring(root.length() - 1, root.length());
        if (!endChar.equals(File.separator)) {
            root = root + File.separatorChar;
        }
        File dirFile = new File(root);
        this.filename = location;
        file = new File(dirFile + File.separator + filename);
        try {
            load();
        } catch (IOException ex) {
            Logger.getLogger(YMLfiles.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Create a new YML file using the specified file and the config options.
     *
     * @param file The file to use.
     * @param configOpt The config options to use.
     * @throws IOException If there was problems associated with files.
     */
    public YMLfiles(File file, Plugin plugin) throws IOException {
        this.file = file;
        this.filename = file.getName();
        this.plugin = plugin;
        try {
            load();
        } catch (IOException ex) {
            Logger.getLogger(YMLfiles.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Load the associated YML plugin file.
     *
     * @throws Exception If there was problems reading the file.
     */
    public void load() throws IOException {
        if (file.exists()) {
            config = YamlConfiguration.loadConfiguration(file);
        } else {
            MDBase.getBase().log("Trying to create " + file.getAbsoluteFile());
            plugin.getDataFolder().mkdirs();
            file.createNewFile();
            config = YamlConfiguration.loadConfiguration(file);
        }
    }

    /**
     * Gets the config associated with this YML file.
     *
     * @return The YAML configuration.
     */
    public YamlConfiguration getConfig() {
        return config;
    }

    /**
     * Gets the file associated with this YML file object.
     *
     * @return The file where this YML is stored.
     */
    public File getFile() {
        return file;
    }

    /**
     * Gets the extension of this YML file.
     *
     * @return The extension, such as ".yml".
     */
    public String getExtension() {
        String name = file.getName();
        int pos = name.lastIndexOf('.');
        return name.substring(pos + 1);
    }

    /**
     * Gets the string associated with the specified key.
     *
     * @param key The key to search for
     * @return Gets a string, if it was there, or creates it if not.
     */
    public String getString(String key) {
        if (config.getKeys(false).contains(key)) {
            return config.getString(key);
        } else {
            config.createSection(key);
            save();
            return config.getString(key);
        }
    }

    /**
     * Sets the key to the specified string.
     *
     * @param key The key to search.
     * @param value The string to set it to.
     */
    public void setString(String key, String value) {
        if (config.getKeys(false).contains(key)) {
            config.set(key, value);
        } else {
            config.createSection(key);
            config.set(key, value);
        }
        save();
    }
    
    public void setObject(String key, Object value) {
        if (config.getKeys(false).contains(key)) {
            config.set(key, value);
        } else {
            config.createSection(key);
            config.set(key, value);
        }
        save();
    }

    /**
     * Save the YML file to specified location.
     */
    public void save() {
        try {
            config.save(file);
        } catch (IOException ex) {
            Logger.getLogger(YMLfiles.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Gets the list associated with a specified key.
     *
     * @param key The key to the list.
     * @return The list associated with the key.
     */
    public List<?> getList(String key) {
        if (config.getKeys(false).contains(key)) {
            return config.getList(key);
        } else {
            config.createSection(key);
            save();
            return config.getList(key);
        }
    }

    /**
     * Sets the key with the specified list
     *
     * @param key The key to set.
     * @param list The list to set the key to.
     */
    public void setList(String key, List<?> list) {
        config.set(key, list);
        save();
    }
    
    public ConfigurationSection getConfigurationSection(String name) {
        ConfigurationSection configurationSection = config.getConfigurationSection(name);
        if (configurationSection == null) {
            MDBase.getBase().log(getFile().getAbsolutePath());
            return config.createSection(name);
        }
        return getConfig().getConfigurationSection(name);
    }
}
