/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.dataobjects.yml;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.mdbase.MDBase;
import org.bukkit.configuration.ConfigurationSection;

/**
 *
 * @author charles
 */
public class ConfigurationNode {

    private ConfigurationSection parentNode;
    private String currentNode;

    public ConfigurationNode(ConfigurationSection parentNode, String currentNode) {
        if (parentNode == null) {
            try {
                throw new Exception("Parent node cannot be null.  Current Node:" + currentNode);
            } catch (Exception ex) {
                Logger.getLogger(ConfigurationNode.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            this.parentNode = parentNode;
            this.currentNode = currentNode;
        }
    }

    public ConfigurationNode(ConfigurationNode mainNode, String currentNode) {
        this.parentNode = mainNode.getCurrentNode();
        this.currentNode = currentNode;
    }

    public void set(Object value) {
        parentNode.set(this.currentNode, value);
    }

    public Object get() {
//        MDBase.getBase().log(parentNode.getName());
        return parentNode.get(this.currentNode);
    }

    public boolean getBoolean() {
        return parentNode.getBoolean(this.currentNode);
    }

    public String getString() {
        return parentNode.getString(this.currentNode);
    }

    public double getDouble() {
        double aDouble = parentNode.getDouble(this.currentNode);
        return aDouble;
    }

    public int getInt() {
        return parentNode.getInt(this.currentNode);
    }

    public Object getValues() {
        return parentNode.getConfigurationSection(this.currentNode).getValues(false);
    }

    public List<Map<?, ?>> getMapList() {
        return parentNode.getMapList(currentNode);
    }

    public ConfigurationSection getParentNode() {
        return parentNode;
    }

    public ConfigurationSection getCurrentNode() {
        return parentNode.getConfigurationSection(currentNode);
    }

}
