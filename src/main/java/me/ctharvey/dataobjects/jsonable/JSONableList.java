/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.dataobjects.jsonable;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.multiserv.packet.JSONable;
import org.json.simple.JSONObject;

/**
 *
 * @author thronecth
 * @param <T>
 */
public class JSONableList<T extends JSONable> extends ArrayList<JSONable> implements JSONable, Serializable {

    private static final long serialVersionUID = 1L;

    private Class clazz;

    public JSONableList(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject jo = new JSONObject();
        jo.put("jsonlist", this);
        return jo;
    }

    /**
     * Clears the current information and pulls from JSONObject.
     *
     * @param jo
     */
    @Override
    public void fromJSONObject(JSONObject jo) {
        clear();
        JSONableList jlist = (JSONableList) jo.get("jsonlist");
        addAll(jlist);
    }

    @Override
    public void fromString(String in) {
        String[] split = in.split(", ");
        for (String string : split) {
            try {
                T newInstance = (T) clazz.newInstance();
                newInstance.fromString(string);
                this.add(newInstance);
            } catch (InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(JSONableList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String prefix = "";
        int cnt = 0;
        for (JSONable json : this) {
            if (cnt > 0) {
                prefix = ", ";
            }
            sb.append(prefix).append(json.toString());
            cnt++;
        }
        return sb.toString();
    }

}
