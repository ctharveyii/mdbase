/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.ctharvey.dataobjects;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

/**
 *
 * @author thronecth
 */
public class SerializableLocation implements ConfigurationSerializable {

	private double x, y, z;
	private String world;

	//Constructor for creating manually
	public SerializableLocation(Location loc) {
		x = loc.getX();
		y = loc.getY();
		z = loc.getZ();
		world = loc.getWorld().getName();
	}

	//Constructor for deserialization
	public SerializableLocation(Map<String, Object> map) {
		x = (Double) map.get("x");
		y = (Double) map.get("y");
		z = (Double) map.get("z");

		world = (String) map.get("world");
	}

	//Function for getting the Location location of this class
	public Location getLocation() {
		World w = Bukkit.getWorld(world);
		if (w == null) {
			return null;
		}
		Location toRet = new Location(w, x, y, z);
		return toRet;
	}

	@Override
	public Map<String, Object> serialize() {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		map.put("x", x);
		map.put("y", y);
		map.put("z", z);
		map.put("world", world);
		return map;
	}

    @Override
    public boolean equals(Object o) {
        if(o instanceof SerializableLocation){
            SerializableLocation loc = (SerializableLocation) o;
            if(loc.getLocation().equals(this.getLocation())){
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.z) ^ (Double.doubleToLongBits(this.z) >>> 32));
        hash = 41 * hash + Objects.hashCode(this.world);
        return hash;
    }
        
        
}
